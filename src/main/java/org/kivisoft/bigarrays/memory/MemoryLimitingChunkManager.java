package org.kivisoft.bigarrays.memory;

import org.kivisoft.bigarrays.arrays.BigMatrix;
import org.kivisoft.bigarrays.arrays.Chunk;

public class MemoryLimitingChunkManager extends ChunkManager {

	private long maxMemory;

	private long currentMemory = 0;

	public MemoryLimitingChunkManager(long maxMemory) {
		this.maxMemory = maxMemory;
	}

	@Override
	public void release(BigMatrix big) {
		for (Chunk c : big.getChunks()) {
			if (c == null)
				continue;
			decrease(c.getChunkType().bytesPerValue * c.height * c.width);
		}
	}

	@Override
	public boolean canCreate(int currentCount, int chunkSizeBytes) {
		System.out.println(currentCount + " " + chunkSizeBytes + " " + currentMemory + " " + maxMemory);
		if (currentCount < 10) {
			increase(chunkSizeBytes);
			return true;
		}
		if (currentMemory + chunkSizeBytes < maxMemory) {
			increase(chunkSizeBytes);
			return true;
		}
		return false;
	}

	private synchronized void increase(long amount) {
		currentMemory += amount;
	}

	private synchronized void decrease(long amount) {
		currentMemory -= amount;
	}

	public long getCurrentMemory() {
		return currentMemory;
	}

	public long getMaxMemory() {
		return maxMemory;
	}

}
