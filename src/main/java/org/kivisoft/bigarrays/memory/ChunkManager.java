package org.kivisoft.bigarrays.memory;

import org.kivisoft.bigarrays.arrays.BigMatrix;

public abstract class ChunkManager {

	public abstract void release(BigMatrix big);

	public abstract boolean canCreate(int currentCount, int chunkSizeBytes);
}
