package org.kivisoft.bigarrays.arrays;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.kivisoft.bigarrays.exceptions.MapOutofBounds;
import org.kivisoft.bigarrays.memory.ChunkManager;
import org.kivisoft.bigarrays.memory.NopChunkManager;
import org.kivisoft.bigarrays.utils.Point;

public class BigMatrix {

	private static final Object ROOT_MUTEX = new Object();

	/**
	 * Keeps track of chunks that are currently on the disk.<br>
	 * Performance considerations: this might be a limiting factor. Considering a
	 * situation with terabytes of disk space and not that much available memory.
	 * Memory fills from the unloaded chunk data before disk.
	 * 
	 * Alternative would be to create an interface that would either store the
	 * unloaded chunk data in the list or use a file system.
	 */
	private final LinkedHashSet<Point> unloadedChunks = new LinkedHashSet<>();

	/*
	 * How many chunks are possible to write on disk -1 is infinite.
	 */
	private int maxChunksOnDisk = 0;

	/*
	 * Maximum number of chunks kept in memory at same time.
	 */
	protected int maxChunksLoaded = -1;

	/*
	 * Location on disk for this byte array
	 */
	protected File chunkSwapRoot;

	/*
	 * Chunk that was accessed last time. Decreases lookup time when iterating
	 * chunks
	 */
	protected Chunk current;

	/*
	 * all chunks as an array.
	 */

	// deprecated, no-one needs this. Just use the map.
	protected Chunk[] chunks;

	/*
	 * current count of chunks created
	 */
	private long chunksCreated = 0;

	/*
	 * how many chunks are currently loaded
	 */
	private int chunksLoaded = 0;
	/*
	 * Size of a single array
	 */
	public final int WIDTH;
	/*
	 * Size of a single array
	 */
	public final int HEIGHT;

	/*
	 * Used to mark if disk has been accessed
	 */
	private boolean diskUsed = false;

	/*
	 * Denotes where the [0][0] index is relatively to the actual byte array. true =
	 * actual index matches the byte array that is contained. false= origin is in
	 * the middle. useful when negative indices are required as well This parameter
	 * is not yet used for anything, but I might try that in case there is a need.
	 */
	// private boolean zeroIndexed;// = false;

	/*
	 * How many times this map has been accessed.
	 */
	protected long iteration = 0;

	private final ChunkType type;

	private final Number chunkDefaultValue;

	static boolean deleteOnExit = true;

	private ChunkManager chunkManager;

	private PrimitiveChunkFactory chunkfactory;

	private HashMap<Point, Chunk> existing;

	/*
	 * Is it allowed to initialize big array to such root folder that is not empty.
	 * Using this option may cause problems if there are files with same name
	 * 
	 */
	private static boolean allowNonEmpty = false;

	private static boolean deleteonExit = true;

	/**
	 * Called after chunk is saved, with the byte-count that was saved TODO: should
	 * this be the CHUNK!!
	 */
	private Consumer<Long> chunkSaved = (byteCount) -> {
	};

	/*
	 * Saves the chunk on disk
	 */
	private Consumer<Chunk> saver;

	private final Consumer<Chunk> firstSaver = (chunk) -> {
		if (chunkSwapRoot == null)
			throw new RuntimeException(new IOException("Swap needed, but no file set"));
		chunkSwapRoot.mkdirs();
		saver = this::saveChunk;
		saver.accept(chunk);
		chunk.disconnect();
	};

	// -----------------CONSTRUCTORS-----------------//
	/*
	 * Initializes a BigByteArray of certain size and id. Using of BBABuilder is
	 * recommended.
	 */
	private BigMatrix(int width, int height, long chunksOnDisk, int chunksInMemory, Number defaultValue, ChunkType type)
			throws RuntimeException {
		this.type = type;

		WIDTH = width % 2 == 0 ? width : width + 1;
		HEIGHT = height % 2 == 0 ? height : height + 1;

		if (chunksInMemory <= 0) {
			chunksInMemory = 500;// TODO: calculate from given memory
		}
		maxChunksLoaded = chunksInMemory;
		chunkDefaultValue = defaultValue;

		init();

	}

	// ---------------- SETUP METHODS------------------//

	/*
	 * initializes some settings
	 */
	private void init() {

		chunks = new Chunk[maxChunksLoaded];
		chunkfactory = new PrimitiveChunkFactory(WIDTH, HEIGHT, chunkDefaultValue, type);
		if (chunkManager == null)
			chunkManager = new NopChunkManager();
		current = loadOrCreateChunk(0, 0);
		existing = new HashMap<>();
		existing.put(current.location(), current);
		saver = firstSaver;
	}

	public void setChunkManager(ChunkManager cm) {
		this.chunkManager = cm;
	}

	private void overFlow() {
		final MapOutofBounds m = new MapOutofBounds();
		m.setContext(this);
		throw m;
	}

	public void setLoadedChunkLimit(final int i) {

		if (i == 0) {
			maxChunksLoaded = 1;
		} else if (i < 0) {
			maxChunksLoaded = -1;
		} else {
			maxChunksLoaded = i;
		}
	}

	/**
	 * sets how many chunks can be stored on the disk for this instance of the
	 * array.
	 *
	 * @param i
	 */
	public void setDiskChunkLimit(int i) {
		if (i < 0) {
			i = -1;
		}
		maxChunksOnDisk = i;
	}

	// ----------------METHODS------------------//

	/**
	 * sets data to a single field. Can be slow if data is set randomly, because all
	 * the chunks have to be checked until correct is found. 
	 *
	 * @param x
	 * @param y
	 */
	public void set(Number value, int x, int y) {
		final Chunk c = findChunk(x, y);
		final int xx = getCoordinateOnChunk(x, c.lowerLeftX);
		final int yy = getCoordinateOnChunk(y, c.lowerLeftY);

		current = c;
		c.setNumber(value, xx, yy);
		c.setLastUsed(iteration);
		iteration++;
	}
/*
	@Deprecated
	public void incrementByte(int x, int y) {

		final Chunk c = findChunk(x, y);

		final int xx = getCoordinateOnChunk(x, c.lowerLeftX);
		final int yy = getCoordinateOnChunk(y, c.lowerLeftY);
		current = c;
		final byte[][] bytes = c.getArray();
		bytes[xx][yy]++;

		c.setLastUsed(iteration);
		iteration++;

	}

	@Deprecated
	public void incrementLong(long val, int x, int y) {
		final Chunk c = getChunk(x, y);

		final int xx = getCoordinateOnChunk(x, c.lowerLeftX);
		final int yy = getCoordinateOnChunk(y, c.lowerLeftY);
		final long[][] array = c.getArray();
		array[xx][yy] += val;

	}
*/
	public Chunk getChunk(int x, int y) {
		final Chunk c = findChunk(x, y);
		current = c;

		c.setLastUsed(iteration);
		iteration++;
		return c;
	}

	/**
	 * gets a chunk index from a coordinate. Chunks are indexed by their lowest
	 * index from each side.
	 *
	 * This method gets the index by converting the coordinate by rounding down to
	 * the nearest chunk. If a chunk size is 100, it means the lower indices are
	 * -50, -50 so chunk accessing coordinate 0,0 should map into index [50,50] in
	 * the actual chunk.
	 *
	 * @param coordinate
	 * @return index of a chunk
	 */
	protected int getChunkLowerLeft(int coordinate, int size) {
		int chunkIndex;
		if (coordinate >= 0) {

			int intermediate = (coordinate + (size / 2));
			chunkIndex = (coordinate - (intermediate % size));
		} else {
			int intermediate = ((coordinate + 1) + (size / 2));
			chunkIndex = ((coordinate + 1) - (intermediate % size) - size);
			if (intermediate > 0) {
				chunkIndex += size;
			}
		}
		return chunkIndex;
	}

	/**
	 * returns the index of a cell in the specific chunk.
	 *
	 * @param coordinate
	 * @param chunkLowerLeft
	 * @return
	 */
	public int getCoordinateOnChunk(int coordinate, int chunkLowerLeft) {
		if (coordinate >= 0)
			return coordinate - chunkLowerLeft;
		return (chunkLowerLeft * -1) - (coordinate * -1);

	}

	/*
	 * Returns a chunk where certain x y coordinate lies.
	 */
	private Chunk findChunk(int x, int y) {
		if (current.contains(x, y))
			return current;
		// special cases where it is within the current's neighbor.
		if (current.up != null && current.up.contains(x, y))
			return current.up;
		if (current.down != null && current.down.contains(x, y))
			return current.down;
		if (current.left != null && current.left.contains(x, y))
			return current.left;
		if (current.right != null && current.right.contains(x, y))
			return current.right;

		int chunkIndex = getChunkLowerLeft(x, WIDTH);
		int chunkIndexy = getChunkLowerLeft(y, HEIGHT);

		// iterate rest of the chunks
		Point index = new Point(chunkIndex, chunkIndexy);
		if (existing.containsKey(index)) {
			return existing.get(index);
		}

		// not found, so we shall make a new one.
		final Chunk c = loadOrCreateChunk(chunkIndex, chunkIndexy);
		existing.put(c.location(), c);
		connectChunkReferences(c);
		return c;

	}

	/**
	 * returns the array of currently loaded chunks
	 *
	 * @return
	 */
	public Chunk[] getChunks() {
		return chunks;
	}

	/**
	 * Connects a chunk to adjacent chunks. Usually called when chunk is loaded or
	 * created.
	 *
	 * @param c
	 */
	protected void connectChunkReferences(Chunk c) {
		Point up = new Point(c.lowerLeftX, c.lowerLeftY + c.height);
		Point down = new Point(c.lowerLeftX, c.lowerLeftY - c.height);
		Point left = new Point(c.lowerLeftX - c.width, c.lowerLeftY);
		Point right = new Point(c.lowerLeftX + c.width, c.lowerLeftY);

		if (existing.containsKey(down)) {
			Chunk k = existing.get(down);
			c.down = k;
			k.up = c;
		}

		if (existing.containsKey(up)) {
			Chunk k = existing.get(up);
			c.up = k;
			k.down = c;
		}

		if (existing.containsKey(right)) {
			Chunk k = existing.get(right);
			c.right = k;
			k.left = c;
		}

		if (existing.containsKey(left)) {
			Chunk k = existing.get(left);
			c.left = k;
			k.right = c;
		}
	}

	/**
	 * Connects a chunk to adjacent chunks. Usually called when chunk is loaded or
	 * created.
	 *
	 * @param c
	 */
	protected void connectChunkReferencesOld(Chunk c) {
		int x = c.lowerLeftX;
		int y = c.lowerLeftY;
		for (Chunk cn : chunks) {
			if (cn == null) {
				continue;
			}

			if (cn.lowerLeftX == x - WIDTH && cn.lowerLeftY == y) {
				cn.right = c;
				c.left = cn;
				// System.out.println("connected " + cn + "<->" + c);
			}
			if (cn.lowerLeftX == x + WIDTH && cn.lowerLeftY == y) {
				cn.left = c;
				c.right = cn;
				// System.out.println("connected " + cn + "<->" + c);
			}
			if (cn.lowerLeftY == y - HEIGHT && cn.lowerLeftX == x) {
				cn.up = c;
				c.down = cn;
				// System.out.println("connected " + cn + "<->" + c);
			}
			if (cn.lowerLeftY == y + HEIGHT && cn.lowerLeftX == x) {
				cn.down = c;
				c.up = cn;
				// System.out.println("connected " + cn + "<->" + c);
			}
		}
	}

	/**
	 * Removes all such chunks that are empty (which contain only zeros)
	 */
	public void cleanLoadedChunks() {
		for (int i = 0; i < chunks.length; i++) {
			final Chunk cn = chunks[i];

			if (cn == null) {
				continue;
			}

			// if chunk is to be deleted, remove references from adjacent chunks
			if (cn.isEmpty()) {
				chunks[i].disconnect();
				chunks[i] = null;
			}
		}

	}

	/**
	 * loads a chunk based on coordinates, or creates empty
	 *
	 * @param path
	 * @return
	 * @throws Exception
	 */
	protected Chunk loadOrCreateChunk(int x, int y) {
		final int chunkX = getChunkLowerLeft(x, WIDTH);
		final int chunkY = getChunkLowerLeft(y, HEIGHT);
		int toLoad = chunksLoaded; // next free spot in the array

		boolean canCreate = chunkManager.canCreate(chunksLoaded, chunkfactory.chunkSizeBytes());

		if (!canCreate || chunksLoaded == chunks.length) {
			if (maxChunksOnDisk != -1 && chunksCreated <= maxChunksOnDisk) {
				overFlow(); // TODO: make sure it works
			}
			int oldestIndex = -1;
			long leastIteration = Long.MAX_VALUE;
			for (int i = 0; i < chunks.length; i++) {

				final Chunk c = chunks[i];
				if (c == null) {
					continue;
				}
				if (c.getLastUsed() < leastIteration) {
					oldestIndex = i;
					leastIteration = c.getLastUsed();
				}
			}
			if (oldestIndex != -1) {
				Chunk removed = chunks[oldestIndex];
				saver.accept(removed);
				removed.disconnect();

				existing.remove(removed.location());
				toLoad = oldestIndex;
			}
		} else {
			chunksLoaded++;
		}

		// chunk is created for the index

		// if disk has not been accessed yet or the new chunk is not listed, no
		// need to try and load a file
		Chunk c;
		if (diskUsed && unloadedChunks.contains(new Point(chunkX, chunkY))) {
			final File f = getFile(chunkX, chunkY);
			// if disk is used and specific file is found
			// if (f.exists()){
			final byte[] values = loadData(f.toPath());
			c = Chunk.fromBytes(values);
			diskUsed = true;
			unloadedChunks.remove(new Point(chunkX, chunkY));

		} else {
			c = chunkfactory.preInitialized(chunkX, chunkY); // empty
		}
		chunksCreated++;
		chunks[toLoad] = c;
		return c;
	}

	private File getFile(int chunkX, int chunkY) {
		final File f = Paths.get(chunkSwapRoot.getAbsolutePath(), chunkX + "," + chunkY).toFile();
		return f;
	}

	private byte[] loadData(Path path) {
		try {
			final byte[] bytes = Files.readAllBytes(path);
			return bytes;
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}

	}

	/**
	 * Writes a single chunk to disk.
	 *
	 * @param chunk
	 * @param fileName
	 */
	private void saveChunk(Chunk c) {
		diskUsed = true;
		File f = new File(chunkSwapRoot + "/" + c.lowerLeftX + "," + c.lowerLeftY);
		unloadedChunks.add(new Point(c.lowerLeftX, c.lowerLeftY));

		try {
			FileOutputStream fos = new FileOutputStream(f);
			fos.write(c.getBytes());
			fos.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		// System.out.println("Saved!");
	}

	public boolean isDiskUsed() {
		return diskUsed;
	}


	public void deleteData() {
		unloadedChunks.stream().map(point -> getFile(point.x, point.y).delete());
		chunkSwapRoot.delete();
		chunkManager.release(this);
		saver = firstSaver;
	}

	/**
	 * Clears all data for this array on the disk
	 *
	 * @param r
	 */
	public void clearData() {
		chunks = new Chunk[maxChunksLoaded];
		current = chunkfactory.preInitialized(0 - WIDTH / 2, 0 - HEIGHT / 2);
		chunks[0] = current;
		chunksLoaded = 1;

		if (!chunkSwapRoot.exists())
			return;
		final File[] fi = chunkSwapRoot.listFiles();
		for (final File c : fi) {
			c.delete();

		}
	}

	/**
	 * Clean loaded chunks and save rest of them to disk.
	 */
	public void dump() throws Exception {
		cleanLoadedChunks();
		for (final Chunk c : chunks) {
			saver.accept(c);
		}
	}

	/**
	 * This can be overwritten if some additional behavior is needed when bytes are
	 * saved on disk.
	 *
	 * @param bytes the amount of bytes written
	 */
	public void chunkSaved(long bytes) {
		chunkSaved.accept(bytes);
	}

	public void setChunkSaved(Consumer<Long> x) {
		this.chunkSaved = x;
	}

	/**
	 * This can be overwritten if new chunk creation requires additional behavior
	 */
	public void chunkCreated() {

	}

	// public void trim(int minx, int miny, int maxx, int maxy) {
	// todo: for all chunks on disk, if they are not within area, delete.
	// todo: for all chunks on disk, if they are partially on area, set
	// remaining parts to zero.
	// System.out.println("TODO: trim not implemented");
	// }

	/**
	 *
	 * @return chunks that are on disk as a stream
	 */
	public Stream<Chunk> streamUnloadedChunks() {
		return unloadedChunks.stream().map(point -> {
			final File file = getFile(point.x, point.y);
			return file;
		}).map(file -> {
			try {
				return Chunk.fromBytes(Files.readAllBytes(file.toPath())); // yeah
																			// horrible
																			// to
																			// handle
																			// exceptions
																			// here..
																			// TODO
			} catch (final IOException e1) {
				e1.printStackTrace();
			}
			return null;
		});
	}

	/**
	 * Call this to make sure there exists chunk in certain location
	 *
	 * @param x
	 * @param y
	 */
	public Chunk assureLocation(int x, int y) {
		return getChunk(x, y);
	}

	/**
	 *
	 * @return default value.
	 */
	public Number getDefaultValue() {
		return chunkDefaultValue;
	}

	/**
	 *
	 * @param x
	 * @param y
	 * @return a number from location
	 */
	public Number getNumber(int x, int y) {
		final Chunk c = getChunk(x, y);
		final int xx = getCoordinateOnChunk(x, c.lowerLeftX);
		final int yy = getCoordinateOnChunk(y, c.lowerLeftY);
		return c.getNumber(xx, yy);
	}

	/**
	 * Sets number to certain location
	 *
	 * @param n
	 * @param x
	 * @param y
	 */
	// public void setNumber(Number n, int x, int y){
	// final Chunk c = getChunk(x, y);
	// final int xx = getCoordinateOnChunk(x, c.lowerLeftX);
	// final int yy = getCoordinateOnChunk(y, c.lowerLeftY);
	// c.setNumber(n, xx, yy);
	// }

	/**
	 *
	 * @return All chunks that are currently on disk
	 */
	public Set<Point> getUnloadedChunks() {
		return unloadedChunks;
	}

	/**
	 * TODO: calculates the minimum indexable value. This is not integer.Min,
	 * because the chunk that Integer.MIN falls on, would have even lower index.
	 * 
	 * @return
	 */
	// public int getMinimumIndex() {
	// return -1;
	// }

	/**
	 *
	 * @return bytes of data on disk, excludes header. Does not take into account
	 *         any file system properties.
	 */
	public long diskBytes() {
		return type.bytesPerValue * HEIGHT * WIDTH * (long) unloadedChunks.size();
	}

	/**
	 * Returns how many bytes used for data currently. Does not take into account
	 * any other data of the object.
	 * 
	 * @return bytes used for data currently
	 */
	public long memBytes() {
		return type.bytesPerValue * HEIGHT * WIDTH * (long) chunksLoaded;
	}

	// TODO: method returns true if this array currently has an index for given
	// coordinate: does not create a new chunk. This is mainly useful for
	// testing or
	// content comparison, where the contents must be compared without creating
	// anything new.
	// This is going to be a bit slow, if every possible coordinate is compared
	// to
	// public boolean hasCoordinate(int x, int y){
	// return false;
	// }

	// ---------------- SWAPPINGPOLICIES -----------------------//

	/*
	 * These are not currently in use.
	 */
	public enum SwappingPolicy {
		LRU, // Least recently used
		FIFO, // first in first out
		RR; // random replacement
	}

	// ---------------- STATIC METHODS -----------------------//

	static HashSet<File> reservedFiles = new HashSet<>();

	private static void reserveFile(File f) throws IOException {
		if (!hasFile(f)) {
			reservedFiles.add(f);
		} else {
			throw new IOException("Duplicate root folder:" + f.getAbsolutePath());
		}

	}

	private static boolean hasFile(File f) {
		return reservedFiles.contains(f);
	}

	private void setRoot(File swapDirectory) {
		synchronized (ROOT_MUTEX) {
			try {
				reserveFile(swapDirectory);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			chunkSwapRoot = swapDirectory;

			if (swapDirectory.exists()) {
				if (!swapDirectory.isDirectory()) {
					throw new RuntimeException(new IOException("Not a directory:" + swapDirectory));
				}
				if (swapDirectory.listFiles().length > 0 && !allowNonEmpty) {
					throw new RuntimeException(new IOException("Target directory must be empty!"));
				}
			}
		}
	}

	public static class Builder {

		protected int w = 100;
		protected int h = 100;
		protected long maxChunksOnDisk = 0;
		protected int maxChunksLoaded = -1;
		private Number defaultvalue = 0;
		private ChunkType chunkType = ChunkType.INT;
		private File root;

		public Builder setWidth(int w) {
			this.w = w;
			return this;
		}

		public Builder setHeight(int h) {
			this.h = h;
			return this;
		}

		public Builder setSize(int s) {
			return this.setHeight(s).setWidth(s);
		}

		public Builder setRoot(File root) {
			this.root = root;
			return this;
		}

		public Builder setDefaultValue(Number b) {
			this.defaultvalue = b;
			return this;
		}

		public Builder setMaxChunksOnDisk(int maxChunksOnDisk) {
			this.maxChunksOnDisk = maxChunksOnDisk;
			return this;
		}

		public Builder setChunkType(ChunkType c) {
			this.chunkType = c;
			return this;
		}

		public Builder setMaxChunksLoaded(int maxChunksLoaded) {
			this.maxChunksLoaded = maxChunksLoaded;
			return this;
		}

		public BigMatrix build() {
			BigMatrix b = new BigMatrix(w, h, maxChunksOnDisk, maxChunksLoaded, defaultvalue, chunkType);
			if (root != null) {
				b.setRoot(root);
			}
			return b;
		}
	}
}
