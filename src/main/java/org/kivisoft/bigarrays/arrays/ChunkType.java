package org.kivisoft.bigarrays.arrays;

import java.util.Arrays;
import java.util.Optional;

public enum ChunkType {
	BYTE(0, 1),
	SHORT(1, Short.BYTES),
	INT(2, Integer.BYTES),
	LONG(3, Long.BYTES),
	FLOAT(5, Float.BYTES),
	DOUBLE(6, Double.BYTES);
	// OBJECT(6, Double.BYTES); bytesPerValue becomes almost unusable and must
	// be refactored out.

	public final byte typeMagic;
	public final int bytesPerValue;

	private ChunkType(int i, int bytes){
		typeMagic = (byte) i;
		this.bytesPerValue = bytes;
	}

	public static Optional<ChunkType> valueOf(int magicType){
		return Arrays.stream(values()).filter(typeNumber -> typeNumber.typeMagic == magicType)
				.findFirst();
	}
	
}
