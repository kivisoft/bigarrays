package org.kivisoft.bigarrays.exceptions;

import org.kivisoft.bigarrays.arrays.BigMatrix;

public class MapOutofBounds extends RuntimeException {

	private BigMatrix context;

	public void setContext(BigMatrix context){
		this.context = context;
	}

	public BigMatrix getContext(){
		return context;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
