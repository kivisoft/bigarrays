package org.kivisoft.chunktest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.Random;

import org.junit.jupiter.api.Test;
import org.kivisoft.bigarrays.arrays.Chunk;
import org.kivisoft.bigarrays.arrays.ChunkType;
import org.kivisoft.bigarrays.arrays.PrimitiveChunkFactory;

import net.jqwik.api.Arbitraries;
import net.jqwik.api.ForAll;
import net.jqwik.api.Property;

public class ChunkComparisonTest {

	@Property
	public void two_chunks_with_same_parameters_are_equal(@ForAll ChunkType type) {

		Chunk a = PrimitiveChunkFactory.getChunk(0, 0, 100, 100, 0, type, true);
		Chunk b = PrimitiveChunkFactory.getChunk(0, 0, 100, 100, 0, type, true);
		assertEquals(a, b);

	}

	@Test
	public void twoDifferentWidthChunksAreNotEqual() {

		Chunk a = PrimitiveChunkFactory.getChunk(0, 0, 99, 100, 0, ChunkType.INT, true);
		Chunk b = PrimitiveChunkFactory.getChunk(0, 0, 100, 100, 0, ChunkType.INT, true);

		assertNotEquals(a, b);

		a = PrimitiveChunkFactory.getChunk(0, 0, 100, 99, 0, ChunkType.INT, true);
		b = PrimitiveChunkFactory.getChunk(0, 0, 100, 100, 0, ChunkType.INT, true);

		assertNotEquals(a, b);

	}

	@Property
	public void anyTwoDifferentChunkTypesAreNotEqual(@ForAll ChunkType type1, @ForAll ChunkType type2) {

		if (type1.equals(type2))
			return;
		Chunk a = PrimitiveChunkFactory.getChunk(0, 0, 100, 100, 0, type1, true);
		Chunk b = PrimitiveChunkFactory.getChunk(0, 0, 100, 100, 0, type2, true);

		assertNotEquals(a, b);
	}

	@Property
	public void chunkContentEquals(@ForAll ChunkType c) {
		int size = 100;

		Chunk a = PrimitiveChunkFactory.getChunk(0, 0, size, size, 0, c, true);
		Chunk b = PrimitiveChunkFactory.getChunk(0, 0, size, size, 0, c, true);

		Random r = new Random(1);

		for (int i = 0; i < 1000; i++) {
			int rand = r.nextInt(10000);
			int x = r.nextInt(size);
			int y = r.nextInt(size);
			a.setNumber(rand, x, y);
			b.setNumber(rand, x, y);
		}
		assertEquals(a, b);

	}

	@Test
	public void compareDifferentContentChunks() {
		int size = 100;
		Chunk a = PrimitiveChunkFactory.getChunk(0, 0, size, size, 0, ChunkType.INT, true);
		Chunk b = PrimitiveChunkFactory.getChunk(0, 0, size, size, 0, ChunkType.INT, true);

		Random r = new Random();
		for (int i = 0; i < 1000; i++) {
			int rand = r.nextInt(10000);

			int x = r.nextInt(size);
			int y = r.nextInt(size);
			a.setNumber(rand, x, y);
			int x2 = r.nextInt(size);
			int y2 = r.nextInt(size);
			b.setNumber(rand, x2, y2);
		}
		assertNotEquals(a, b);
	}
}
