package org.kivisoft.chunktest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.kivisoft.bigarrays.arrays.Chunk;
import org.kivisoft.bigarrays.arrays.ChunkType;
import org.kivisoft.bigarrays.arrays.PrimitiveChunkFactory;

import net.jqwik.api.ForAll;
import net.jqwik.api.Property;

public class PrimitiveTest {

	@Property
	public void aSetValueOnChunkCanBeRetrieved(@ForAll ChunkType c) {

		int size = 100;
		Chunk a = PrimitiveChunkFactory.getChunk(0, 0, size, size, 0, c, true);
		Integer content = 1;
		int x = 5;
		int y = 6;
		a.setNumber(content, x, y);
		assertEquals(a.getNumber(x, y).longValue(), content.longValue());

	}
}
