# BigArrays
Java implementation of chunk based 2D array with dimensions of 2^32 


## Description

BigArray offers an abstraction to save any types of numbers as primitives into a dynamically growing array and can be configured to use disk as additional memory.

### Why Chunk is not generic?

As mentioned in previous section, this needs to be able to store primitives. Using primitives is faster and more memory efficient.

I guess this could be easily extended for generic types as well and I actually might when I have time.

## Known issues

The borders are still undefined behavior. Calling
bigArray.set(number,Integer.MIN_VALUE,INTEGER.MIN_VALUE); will likely crash it due to way chunks are indexed. 

There are also couple of todo's still around.
